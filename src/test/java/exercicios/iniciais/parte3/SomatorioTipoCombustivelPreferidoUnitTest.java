package exercicios.iniciais.parte3;


import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class SomatorioTipoCombustivelPreferidoUnitTest {
	
	@Test
	public void test1ClienteParaCadaTipoCombustivel() {

		SomatorioTipoCombustivelPreferido somatorioTipoCombustivel = new SomatorioTipoCombustivelPreferido();
		
		int alcoolCodigo = 1;
		int dieselCodigo = 2;
		int gasolinaCodigo = 3;
		
		somatorioTipoCombustivel.adicionarEscolhaCombustivelPreferido(alcoolCodigo);
		somatorioTipoCombustivel.adicionarEscolhaCombustivelPreferido(dieselCodigo);
		somatorioTipoCombustivel.adicionarEscolhaCombustivelPreferido(gasolinaCodigo);
		
		assertEquals(1, somatorioTipoCombustivel.alcool);
		assertEquals(1, somatorioTipoCombustivel.diesel);
		assertEquals(1, somatorioTipoCombustivel.gasolina);
	}
	
	@Test
	public void testSomenteAlcool() {

		SomatorioTipoCombustivelPreferido somatorioTipoCombustivel = new SomatorioTipoCombustivelPreferido();
		
		int alcoolCodigo = 1;
		
		somatorioTipoCombustivel.adicionarEscolhaCombustivelPreferido(alcoolCodigo);
		
		assertEquals(1, somatorioTipoCombustivel.alcool);
		assertEquals(0, somatorioTipoCombustivel.diesel);
		assertEquals(0, somatorioTipoCombustivel.gasolina);
	}
	
	@Test
	public void testSomenteGasolina() {

		SomatorioTipoCombustivelPreferido somatorioTipoCombustivel = new SomatorioTipoCombustivelPreferido();
		
		int gasolinaCodigo = 2;
		
		somatorioTipoCombustivel.adicionarEscolhaCombustivelPreferido(gasolinaCodigo);
		
		assertEquals(0, somatorioTipoCombustivel.alcool);
		assertEquals(0, somatorioTipoCombustivel.diesel);
		assertEquals(1, somatorioTipoCombustivel.gasolina);
	}
	
	@Test
	public void testSomenteDiesel() {

		SomatorioTipoCombustivelPreferido somatorioTipoCombustivel = new SomatorioTipoCombustivelPreferido();
		
		int dieselCodigo = 3;
		
		somatorioTipoCombustivel.adicionarEscolhaCombustivelPreferido(dieselCodigo);
		
		assertEquals(0, somatorioTipoCombustivel.alcool);
		assertEquals(1, somatorioTipoCombustivel.diesel);
		assertEquals(0, somatorioTipoCombustivel.gasolina);
	}
	
	@Test
	public void testNenhumCombustivel() {

		SomatorioTipoCombustivelPreferido somatorioTipoCombustivel = new SomatorioTipoCombustivelPreferido();
		
		somatorioTipoCombustivel.adicionarEscolhaCombustivelPreferido(0);
		
		assertEquals(0, somatorioTipoCombustivel.alcool);
		assertEquals(0, somatorioTipoCombustivel.diesel);
		assertEquals(0, somatorioTipoCombustivel.gasolina);
	}

}

