package exercicios.iniciais.parte4;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class FatorialSimplesUnitTest {
	
	@Test
	public void testComValor4() {
		
		FatorialSimples fatorialSimples = new FatorialSimples();
		
		int fatorialCalculado = fatorialSimples.calculaFatorial(4);
		
		assertEquals(24, fatorialCalculado);
	}
	
	@Test
	public void testComValor0() {
		
		FatorialSimples fatorialSimples = new FatorialSimples();
		
		int fatorialCalculado = fatorialSimples.calculaFatorial(0);
		
		assertEquals(0, fatorialCalculado);
	}
	
	@Test
	public void testComValor1() {
		
		FatorialSimples fatorialSimples = new FatorialSimples();
		
		int fatorialCalculado = fatorialSimples.calculaFatorial(1);
		
		assertEquals(1, fatorialCalculado);
	}
	
	@Test
	public void testComValor7() {
		
		FatorialSimples fatorialSimples = new FatorialSimples();
		
		int fatorialCalculado = fatorialSimples.calculaFatorial(7);
		
		assertEquals(5040, fatorialCalculado);
	}

}
