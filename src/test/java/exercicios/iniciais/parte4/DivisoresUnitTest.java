package exercicios.iniciais.parte4;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Test;

public class DivisoresUnitTest {
	
		
	@Test
	public void testComDividendo1() {
		
		Divisores divisores = new Divisores();
		
		List<Integer> resultado = divisores.mostrarDivisores(1);
		
		assertEquals(1, resultado.get(0), 0.0);
		
	}
	
	@Test
	public void testComDividendo2() {
		
		Divisores divisores = new Divisores();
		
		List<Integer> resultado = divisores.mostrarDivisores(2);
		
		assertEquals(1, resultado.get(0), 0.0);
		assertEquals(2, resultado.get(1), 0.0);
		
	}
	
	@Test
	public void testComDividendo6() {
		
		Divisores divisores = new Divisores();
		
		List<Integer> resultado = divisores.mostrarDivisores(6);
		
		assertEquals(1, resultado.get(0), 0.0);
		assertEquals(2, resultado.get(1), 0.0);
		assertEquals(3, resultado.get(2), 0.0);
		assertEquals(6, resultado.get(3), 0.0);
		
	}
	
	@Test
	public void testComDividendo20() {
		
		Divisores divisores = new Divisores();
		
		List<Integer> resultado = divisores.mostrarDivisores(20);
		
		assertEquals(1, resultado.get(0), 0.0);
		assertEquals(2, resultado.get(1), 0.0);
		assertEquals(4, resultado.get(2), 0.0);
		assertEquals(5, resultado.get(3), 0.0);
		assertEquals(10, resultado.get(4), 0.0);
		assertEquals(20, resultado.get(5), 0.0);
		
	}

}
