package exercicios.iniciais.parte2;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class NumNegativoUnitTest {
	
	@Test
	public void testComNumeroPositivo() {
		
		NumNegativo numNegativo = new NumNegativo();
		boolean isNumeroNegativo = numNegativo.verificaNumeroNegativo(0);
		
		assertFalse(isNumeroNegativo);
	}
	
	@Test
	public void testComNumeroNegativo() {
		
		NumNegativo numNegativo = new NumNegativo();
		boolean isNumeroPositivo = numNegativo.verificaNumeroNegativo(-1);
		
		assertTrue(isNumeroPositivo);
	}

}
