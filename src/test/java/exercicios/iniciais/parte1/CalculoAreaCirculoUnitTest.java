package exercicios.iniciais.parte1;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class CalculoAreaCirculoUnitTest {
	
	@Test
	public void testCalcularAreaCirculo() {
		
		CalculoAreaCirculo calculoAreaCirculo = new CalculoAreaCirculo();
		
		double resultadoAreaCirculo = calculoAreaCirculo.calcularAreaCirculo(2.0); 
		
		assertEquals(12.5664, resultadoAreaCirculo, 0.0001);
		
	}

}
