package salario;

import java.util.Locale;
import java.util.Scanner;

import salario.entities.Employee;

public class Program {

	public static void main(String[] args) {
		
		Locale.setDefault(Locale.US);
		Scanner sc = new Scanner(System.in);
		
		double percentage = 0.0;
		
		Employee employee = new Employee();
		
		System.out.println("Enter employee data:");
		System.out.print("Name: ");
		employee.name = sc.nextLine();
		System.out.print("Gross salary: ");
		employee.grossSalary = sc.nextDouble();
		System.out.print("Tax: ");
		employee.tax = sc.nextDouble();
		System.out.println();
		
		System.out.println("Employee: " + employee);
		System.out.println();
		
		System.out.print("Which percentage to increase salary? ");
		percentage = sc.nextDouble();
		employee.increaseSalary(percentage);
		System.out.println();
		
		System.out.print("Updated data: " + employee);
		
		sc.close();

	}

}
