package list1;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;

import list1.entities.Employee;

public class Program {

	public static void main(String[] args) {
		
		Locale.setDefault(Locale.US);
		Scanner sc = new Scanner(System.in);
		List<Employee> employees = new ArrayList<>();
		
		System.out.print("How many employees will be registered? ");
		int quantidadeFuncionarios = sc.nextInt();
		System.out.println();
		
		getEmployeeInfo(sc, employees, quantidadeFuncionarios);
		
		System.out.print("Enter de employee id that will have salary increase: ");
		Integer id = sc.nextInt();
		
		Employee employeeFound = getEmployeeById(employees, id);
		
		if (employeeFound == null) {
			System.out.println("This id doesn't exist!");
		}
		else {
			System.out.print("Enter the percentage: ");
			Double percentage = sc.nextDouble();
			employeeFound.increaseSalary(percentage);
		}
		
		System.out.println();
		System.out.println("List of employees:");
		showFormatInfo(employees);
		
		sc.close();
	}
	
	private static void getEmployeeInfo(Scanner sc, List<Employee> employees, int quantidadeFuncionarios) {
		for (int i = 0; i < quantidadeFuncionarios; i++) {
			System.out.println("Employee #" + (i+1) + ":");
			System.out.print("Id: ");
			Integer id = sc.nextInt();
			sc.nextLine();
			System.out.print("Name: ");
			String name = sc.nextLine();	
			System.out.print("Salary: ");
			Double salary = sc.nextDouble();
			System.out.println();
			Employee employee = new Employee(id, name, salary);
			employees.add(employee);
		}
	}
	
	private static Employee getEmployeeById(List<Employee> employees, Integer id) {
		for (Employee employee : employees) {
			if(employee.getId().equals(id)) {
				return employee;
			}
		}
		return null; //TODO refatorar - lançar excessao especifica
	}
	
	private static void showFormatInfo(List<Employee> employees) {
		for (Employee employee : employees) {
			System.out.println(employee.getFormatInfos());
		}
	}
}
