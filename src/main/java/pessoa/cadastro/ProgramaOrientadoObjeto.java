package pessoa.cadastro;

import java.util.Scanner;

import pessoa.cadastro.domains.Pessoa;

public class ProgramaOrientadoObjeto {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		System.out.print("Quantas pessoas serão cadastradas? ");
		int tamanhoVetor = sc.nextInt();
		
		Pessoa[] pessoas = new Pessoa[tamanhoVetor];

		System.out.println();

		for (int i = 0; i < pessoas.length; i++) {
			pessoas[i] = new Pessoa();
			
			System.out.print("Nome: ");
			pessoas[i].nome = sc.next();

			System.out.print("Idade: ");
			pessoas[i].idade = sc.nextInt();

			System.out.print("Telefone: ");
			pessoas[i].telefone = sc.next();

			System.out.print("CPF: ");
			pessoas[i].cpf = sc.next();
			
			System.out.println();
		}

		System.out.println("Pessoa(s) cadastrada(s) maior de 18 anos:");

		for (int i = 0; i < pessoas.length; i++) {
			if (pessoas[i].isMaiorDeIdade()) {
				System.out.println(pessoas[i].nome);
			}
		}

		sc.close();

	}

}
