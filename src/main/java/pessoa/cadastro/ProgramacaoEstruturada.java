package pessoa.cadastro;

import java.util.Scanner;

/* TODO cadastro de pessoas em array de 10 posições ou menos.
		dados das pessoas: nome, telefone(string), endereço,cpf 
		depois de cadastrar mostrar nome das pessoas cadastradas.*/

public class ProgramacaoEstruturada {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		System.out.print("Quantas pessoas serão cadastradas? ");
		int tamanhoVetor = sc.nextInt();
		String[] nomesPessoas = new String[tamanhoVetor];
		int[] idadePessoas = new int[tamanhoVetor];
		String[] telefonePessoas = new String[tamanhoVetor];
		String[] cpfPessoas = new String[tamanhoVetor];
		
		System.out.println();
		
		for (int i = 0; i < nomesPessoas.length; i++) {
			System.out.print("Nome: ");
			nomesPessoas[i] = sc.next();
			
			System.out.print("Nome: ");
			idadePessoas[i] = sc.nextInt();
			
			System.out.print("Telefone: ");
			telefonePessoas[i] = sc.next();
			
			System.out.print("CPF: ");
			cpfPessoas[i] = sc.next();
			System.out.println();
		}
		
		System.out.println("Pessoa(s) cadastrada(s)");
		
		for (int i = 0; i < nomesPessoas.length; i++) {
			if (idadePessoas[i] > 18) {
				System.out.println(nomesPessoas[i]);
			}
		}
		
		sc.close();

	}

}
