package hostel;

import java.util.Scanner;

import hostel.entities.Rent;

public class Program {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		Rent[] rooms = new Rent[10];
		
		System.out.print("How many rooms will be rented? ");
		int quantityOfRoomsRented = sc.nextInt();
		System.out.println();
		
		rentRoom(quantityOfRoomsRented, rooms, sc);
		
		System.out.println("Busy rooms:");
		
		listBusyRooms(rooms);
		
		sc.close();
	}
	
	private static void rentRoom(int quantityOfRoomsRented, Rent[] rooms, Scanner sc) {
		for (int i = 0; i < quantityOfRoomsRented; i++) {
			System.out.println("Rent #"+ (i+1)+ ":");
			sc.nextLine();
			System.out.print("Name: ");
			String name = sc.nextLine();
			System.out.print("Email: ");
			String email = sc.next();
			System.out.print("Room: ");
			int roomNumber = sc.nextInt();
			System.out.println();
			
			rooms[roomNumber] = new Rent(name, email);
		}
	}
	
	private static void listBusyRooms(Rent[] rooms) {
		for (int i = 0; i < rooms.length; i++) {
			if (rooms[i] != null) {
				System.out.println(i + ": " + rooms[i].getInfo());
			}
		}
	}

}
