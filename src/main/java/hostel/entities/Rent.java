package hostel.entities;

public class Rent {
	
	private String name;
	private String email;
	
	public Rent(String studentName, String studentEmail) {
		this.name = studentName;
		this.email = studentEmail;
	}

	public String getStudentName() {
		return name;
	}

	public void setStudentName(String studentName) {
		this.name = studentName;
	}

	public String getStudentEmail() {
		return email;
	}

	public void setStudentEmail(String studentEmail) {
		this.email = studentEmail;
	}

	public String getInfo() {
		return name + ", " + email;
	}
	
}
