package cotacao.dolar.util;

public class CurrencyConverter {
	
	public static final double IOF = 0.06;
	public static double quote;
	public static double amountInDollar;
	
	public static double amountToBePaidInReais() {
		return (amountInDollar + (amountInDollar * IOF)) * quote;
	}

}
