package nota.trimestral;

import java.util.Locale;
import java.util.Scanner;

import nota.trimestral.domains.Student;

public class Program {

	public static void main(String[] args) {
		
		Locale.setDefault(Locale.US);
		Scanner sc = new Scanner(System.in);
		
		Student student = new Student();
		
		System.out.print("Student name: ");
		student.studentName = sc.nextLine();
		System.out.println("Enter grade 1, 2, 3:");
		student.grade1 = sc.nextDouble();
		student.grade2 = sc.nextDouble();
		student.grade3 = sc.nextDouble();
		
		System.out.printf("FINAL GRADE = %.2f%n", student.finalGrade());
		
		if (student.isApproved() == false) {
			System.out.println("FAILED"); 
			System.out.printf("MISSING %.2f POINTS", student.missingPoints());
		}
		else {
			System.out.println("PASS");
		}
		
		sc.close();

	}

}
