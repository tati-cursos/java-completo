package nota.trimestral.domains;

public class Student {
	
	public String studentName;
	public double grade1;
	public double grade2;
	public double grade3;
	
	public double finalGrade() {
		return grade1 + grade2 + grade3;
	}
	
	public boolean isApproved() {
		if (finalGrade() < 60) {
			return false;
		}
		else {
			return true;
		}
	}
	
	public double missingPoints() {
		return 60 - finalGrade();
	}
	
	
//	public void checkPassOrFail() {
//		if(finalGrade() >= 60) {
//			System.out.println("PASS");
//		}
//		else {
//			System.out.println("FAILED");
//			double missing = 60 - finalGrade();
//			System.out.printf("MISSING %.2f POINTS" , missing);
//		}
//	}
	
}
