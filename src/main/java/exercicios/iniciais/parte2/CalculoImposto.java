package exercicios.iniciais.parte2;

import java.util.Locale;
import java.util.Scanner;

public class CalculoImposto {

	public static void main(String[] args) {
		
		Locale.setDefault(Locale.US);
		Scanner sc = new Scanner(System.in);
		
		double salario;
		double valorIsento = 2000.00;
		double faixa1 = 0.08;
		double faixa2 = 0.18;
		double faixa3 = 0.28; 
		double valorFaixa1 = 999.99;
		double valorFaixa2 = 1499.99;
		double desconto;
		
		System.out.println("Informe o salário:");
		salario = sc.nextDouble();		
		salario -= valorIsento;

		if (salario > 0 && salario <= valorFaixa1) {
			desconto = salario * faixa1;
			System.out.printf("Desconto IR = R$ %.2f%n", desconto);
			
		} else if (salario > valorFaixa1 && salario <= valorFaixa2) {
			desconto = ((salario - valorFaixa1) * faixa2) + (valorFaixa1 * faixa1);
			System.out.printf("Desconto IR = R$ %.2f%n", desconto);
		
		} else if (salario > valorFaixa2) {
			desconto = (salario - valorFaixa1 - valorFaixa2) * faixa3;
			desconto += (valorFaixa1 * faixa1) + (valorFaixa2 * faixa2);
			System.out.printf("Desconto IR = R$ %.2f%n", desconto);
		}
		else {
			System.out.println("ISENTO do IR.");
		}
				
		sc.close();

	}

}
