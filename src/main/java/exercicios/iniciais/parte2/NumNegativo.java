package exercicios.iniciais.parte2;

public class NumNegativo {

	public boolean verificaNumeroNegativo(int numeroInteiro) {
		
		if (numeroInteiro < 0) {
			return true;
		}
		return false;
	}
}
