package exercicios.iniciais.parte2;

import java.util.Locale;
import java.util.Scanner;

public class ValorDaConta {

	public static void main(String[] args) {
		
		Locale.setDefault(Locale.US);
		Scanner sc = new Scanner(System.in);
		
		double cachorroQuente = 4.00;
		double xSalada = 4.50;
		double xBacon = 5.00;
		double torradaSimples = 2.00;
		double refrigerante = 1.50;
		double valorTotal = 0;
		int codigoProduto;
		int qtdProduto;
		
		System.out.println("Informe código do produto e quantidade:");
		codigoProduto = sc.nextInt();
		qtdProduto = sc.nextInt();
		
		if (codigoProduto == 1) {
			valorTotal = cachorroQuente * qtdProduto;
		}
		else if (codigoProduto == 2) {
			valorTotal = xSalada * qtdProduto;
		}
		else if (codigoProduto == 3) {
			valorTotal = xBacon * qtdProduto;
		}
		else if (codigoProduto == 4) {
			valorTotal = torradaSimples * qtdProduto;
		}
		else if (codigoProduto == 5) {
			valorTotal = refrigerante * qtdProduto;
		}
		else {
			System.out.println("Valor inválido, tente novamente.");
		}
		
		System.out.printf("Total: R$ %.2f%n", valorTotal);
		
		sc.close();

	}

}
