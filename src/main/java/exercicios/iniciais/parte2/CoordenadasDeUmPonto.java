package exercicios.iniciais.parte2;

import java.util.Locale;
import java.util.Scanner;

public class CoordenadasDeUmPonto {

	public static void main(String[] args) {
		
		Locale.setDefault(Locale.US);
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Digite 2 valores com 1 casa decimal, que representam coordenadas em um plano cartesiano.");
		
		double x;
		double y;
		x = sc.nextDouble();
		y = sc.nextDouble();
		
		if (x > 0 && y > 0) {
			System.out.println("Quadrante 1");
		}
		else if (x > 0 && y < 0) {
			System.out.println("Quadrante 4");
		}
		else if (x < 0 && y < 0) {
			System.out.println("Quadrante 3");
		}
		else if (x < 0 && y > 0) {
			System.out.println("Quadrante 2");
		}
		else {
			System.out.println("Origem");
		}
		
		sc.close();
	}

}
