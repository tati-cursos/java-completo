package exercicios.iniciais.parte2;

import java.util.Scanner;

public class DuracaoJogo {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Informe hora início e fim do jogo.");
		System.out.println("obs.: Não informar minutos.");
		int inicio;
		int fim;
		int duracao = 0;
		
		inicio = sc.nextInt();
		fim = sc.nextInt();
		
		if (inicio == 0 && fim == 0) {
			duracao = 24;
		}
		else if (inicio > fim) {
			duracao = (24 - inicio) + fim;
		}
		else {
			duracao = fim - inicio;
		}
		
		System.out.println("O jogo durou " + duracao + " hora(s).");
		
		sc.close();

	}

}
