package exercicios.iniciais.parte3;

import java.util.Scanner;

public class matrix1 {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Qual o tamanho da matriz?");
		System.out.print("linhas: ");
		int qtdLinhas = sc.nextInt();
		System.out.print("colunas: ");
		int qtdColunas = sc.nextInt();
		
		System.out.println("Digite números inteiros para preencher a matriz:");
		int[][] mat = new int[qtdLinhas][qtdColunas];
		
		for (int i = 0; i < mat.length; i++) {
			for (int j = 0; j < mat[i].length; j++) {
				mat[i][j] = sc.nextInt();
			}
		}
		System.out.print("Informe um número contido na matriz: ");
		int numeroDaMatriz = sc.nextInt();
		
		for (int i = 0; i < mat.length; i++) {
			for (int j = 0; j < mat[i].length; j++) {
				if (numeroDaMatriz == mat[i][j]) {
					System.out.println("Position: " + i + "," + j);
					
					if (j > 0) {
						System.out.println("Left: "+ mat[i][j-1]);
					}
					if (j < mat[i].length-1) {
						System.out.println("Right: "+ mat[i][j+1]);
					}
					if (i > 0) {
						System.out.println("Up: "+ mat[i-1][j]);
					}
					if (i < mat.length-1) {
						System.out.println("Down: "+ mat[i+1][j]);
					}
				}
			}
		}
		sc.close();
	}

}
