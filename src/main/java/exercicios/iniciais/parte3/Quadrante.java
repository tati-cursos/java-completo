package exercicios.iniciais.parte3;

import java.util.Scanner;

public class Quadrante {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		int x;
		int y;

		System.out.println("Digite 2 valores que representam coordenadas em um plano cartesiano.");
		x = sc.nextInt();
		y = sc.nextInt();

		while (x != 0 && y != 0) {
			if (x > 0 && y > 0) {
				System.out.println("Quadrante 1");
			} else if (x < 0 && y > 0) {
				System.out.println("Quadrante 2");
			} else if (x < 0 && y < 0) {
				System.out.println("Quadrante 3");
			} else {
				System.out.println("Quadrante 4");
			}

			x = sc.nextInt();
			y = sc.nextInt();
		}
		
		System.out.println("Fim");

		sc.close();

	}

}
