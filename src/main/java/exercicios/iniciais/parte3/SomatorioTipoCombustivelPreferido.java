package exercicios.iniciais.parte3;

public class SomatorioTipoCombustivelPreferido {

	public int alcool = 0;
	public int gasolina = 0;
	public int diesel = 0;
	
	public void adicionarEscolhaCombustivelPreferido(int codigoCombustivel) {

		if (codigoCombustivel == 1) {
			alcool += 1;
		}
		else if (codigoCombustivel == 2) {
			gasolina += 1;
		}
		else if (codigoCombustivel == 3) {
			diesel += 1;
		}
	}

}
