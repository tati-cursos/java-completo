package exercicios.iniciais.parte3;

import java.util.Scanner;

public class SomatorioTipoCombustivelApp {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		SomatorioTipoCombustivelPreferido somatorioTipoCombustivelPreferido = new SomatorioTipoCombustivelPreferido();
		
		int codigoCombustivel = sc.nextInt();
			
		while (codigoCombustivel != 4) {
			
			somatorioTipoCombustivelPreferido.adicionarEscolhaCombustivelPreferido(codigoCombustivel);
			codigoCombustivel = sc.nextInt();
			
		}
		
		System.out.println("Alcool = " + somatorioTipoCombustivelPreferido.alcool);
		System.out.println("Gasolina = " + somatorioTipoCombustivelPreferido.gasolina);
		System.out.println("Diesel = " + somatorioTipoCombustivelPreferido.diesel);
		
		sc.close();

	}

}
