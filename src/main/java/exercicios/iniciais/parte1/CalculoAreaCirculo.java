package exercicios.iniciais.parte1;

public class CalculoAreaCirculo {

	public double calcularAreaCirculo(double raio) {
		
		double area = Math.PI * (Math.pow(raio, 2));

		return area;

	}

}
