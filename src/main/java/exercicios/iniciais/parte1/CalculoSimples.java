package exercicios.iniciais.parte1;

import java.awt.geom.QuadCurve2D;
import java.util.Locale;
import java.util.Scanner;

public class CalculoSimples {

	public static void main(String[] args) {
		
		Locale.setDefault(Locale.US);
		Scanner sc = new Scanner(System.in);
		
		int codigoPeca1;
		int qtdPeca1;
		double valorUnitarioPeca1;
		int codigoPeca2;
		int qtdPeca2;
		double valorUnitarioPeca2;
		double valorTotal;
		
		codigoPeca1 = sc.nextInt();
		qtdPeca1 = sc.nextInt();
		valorUnitarioPeca1 = sc.nextDouble();
		codigoPeca2 = sc.nextInt();
		qtdPeca2 = sc.nextInt();
		valorUnitarioPeca2 = sc.nextDouble();
		valorTotal = ((qtdPeca1 * valorUnitarioPeca1) + (qtdPeca2 * valorUnitarioPeca2));
		
		System.out.printf("Valor a pagar: R$ %.2f%n ", valorTotal);
		
		sc.close();

	}

}
