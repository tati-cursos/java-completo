package exercicios.iniciais.parte1;

import java.util.Scanner;

public class SomaSimples {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		int x;
		int y;
		int soma = 0;
		
		x = sc.nextInt();
		y = sc.nextInt();
		
		soma = x + y;
		System.out.println("SOMA = " + soma);
		
		sc.close();

	}

}
