package exercicios.iniciais.parte1;

import java.util.Scanner;

public class Diferenca {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		int a;
		int b;
		int c;
		int d;
		int diferenca = 0;
		
		a = sc.nextInt();
		b = sc.nextInt();
		c = sc.nextInt();
		d = sc.nextInt();
		diferenca = ((a*b) - (c*d));
		
		System.out.println("Diferença = " + diferenca);
		
		sc.close();

	}

}
