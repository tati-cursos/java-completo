package exercicios.iniciais.parte1;

import java.util.Locale;
import java.util.Scanner;

public class Salario {

	public static void main(String[] args) {
		
		Locale.setDefault(Locale.US);
		Scanner sc = new Scanner(System.in);
		
		int numero;
		double horasTrabalhadas;
		double valorHora;
		double salario;
		
		numero = sc.nextInt();
		horasTrabalhadas = sc.nextDouble();
		valorHora = sc.nextDouble();
		salario = valorHora * horasTrabalhadas;
		
		System.out.println("Número funcionário = " + numero);
		System.out.println("Salário = U$ " + salario);
		
		sc.close();
	}

}
