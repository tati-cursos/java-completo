package exercicios.iniciais.parte4;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Divisores {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		Divisores divisor = new Divisores();
		
		System.out.println("Digite o dividendo:");
		int dividendo = sc.nextInt();
		
		List<Integer> divisores = divisor.mostrarDivisores(dividendo);
		
		System.out.println(divisores);
		
		sc.close();

	}
	
	public List<Integer> mostrarDivisores(int dividendo) {
		
		List<Integer> divisores = new ArrayList<>();
		
		for (int i = 1; i <= dividendo; i++) {
			if (dividendo % i == 0) {
				divisores.add(i);
			}
		}
		return divisores;
	}

}
