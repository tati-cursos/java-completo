package exercicios.iniciais.parte4;

import java.util.Scanner;

public class PessoaMaisVelha {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		int tamanhoVetor = tamanhoVetor(sc);
		
		String[] nomePessoa = new String[tamanhoVetor]; 
		int[] idadePessoa = new int[tamanhoVetor];
				
		coletarNomeIdadePessoas(tamanhoVetor, sc, nomePessoa, idadePessoa);
		
		verificaPessoaMaisVelha(tamanhoVetor, idadePessoa, nomePessoa);
		
		sc.close();

	}
	
	private static int tamanhoVetor(Scanner sc) {
		int tamanhoVetor = sc.nextInt();
		return tamanhoVetor;
	}
	
	private static void coletarNomeIdadePessoas(int tamanhoVetor, Scanner sc, String[] nomePessoa, int[] idadePessoa) {

		for (int i = 0; i < tamanhoVetor; i++) {
			nomePessoa[i] = sc.next();
			idadePessoa[i] = sc.nextInt();
		}
	}
	
	private static void verificaPessoaMaisVelha(int tamanhoVetor, int[] idadePessoa, String[] nomePessoa) {
		
		int maiorIdade = 0;
		String nomePessoaMaisVelha = "";
		
		for (int i = 0; i < tamanhoVetor; i++) {
			if (idadePessoa[i] > maiorIdade) {
				maiorIdade = idadePessoa[i];
				nomePessoaMaisVelha = nomePessoa[i];
			}
		}
		System.out.println("Pessoa mais velha: "+ nomePessoaMaisVelha);
	}

}
