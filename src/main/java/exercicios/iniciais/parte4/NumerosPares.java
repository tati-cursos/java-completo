package exercicios.iniciais.parte4;

import java.util.Scanner;

/* Leia N números inteiros e armazene-os em um vetor. Em seguida, mostre na tela:
	- todos os números pares
	- a quantidade de números pares */

public class NumerosPares {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		int qtdNumerosInteiros = sc.nextInt();
		int[] numerosInteiros = new int[qtdNumerosInteiros];
		int qtdnumerosPares = 0;
		
		for (int i = 0; i < qtdNumerosInteiros; i++) {
			numerosInteiros[i] = sc.nextInt();
		}
		
		for (int i = 0; i < qtdNumerosInteiros; i++) {
			if (numerosInteiros[i] % 2 == 0) {
				System.out.print(numerosInteiros[i] + " ");
				qtdnumerosPares++;
			}
		}
		
		System.out.println();
		System.out.println("Quantidade de números pares: " + qtdnumerosPares);
		
		sc.close();

	}

}
