package exercicios.iniciais.parte4;

import java.util.Locale;
import java.util.Scanner;

public class IdadeEAltura {

	public static void main(String[] args) {
		
		Locale.setDefault(Locale.US);
		Scanner sc = new Scanner(System.in);
		
		System.out.print("Digite a quantidade de pessoas: ");
		int qtdPessoas = sc.nextInt();
		
		String[] nomePessoas = new String[qtdPessoas];
		int[] idadePessoas = new int[qtdPessoas];
		double[] alturaPessoas = new double[qtdPessoas];
		int somaMenorDe16Anos = 0;
		double percentualMenorDe16Anos = 0.0;
		double somaAltura = 0.0;
		double alturaMedia = 0.0;
		
		System.out.println("Informe nome, idade e altura de cada pessoa:");
		
		for (int i = 0; i < qtdPessoas; i++) {
			nomePessoas[i] = sc.next();
			idadePessoas[i] = sc.nextInt();
			alturaPessoas[i] = sc.nextDouble();
			
			somaAltura += alturaPessoas[i];
			
			if (idadePessoas[i] < 16) {
				somaMenorDe16Anos += 1;
			}
	
		}
		alturaMedia = somaAltura / qtdPessoas;
		percentualMenorDe16Anos = (somaMenorDe16Anos * 100)/qtdPessoas;
		
		System.out.printf("Altura média: %.2f%n", alturaMedia);
		System.out.printf("Pessoas com menos de 16 anos:  %.1f%%%n", percentualMenorDe16Anos);
		
				
		sc.close();

	}

}
