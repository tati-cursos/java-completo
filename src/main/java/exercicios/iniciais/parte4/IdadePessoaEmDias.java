package exercicios.iniciais.parte4;

import java.util.Scanner;

/* leia a idade de uma pessoa expressa em anos, meses e dias e escreva a idade
dessa pessoa expressa apenas em dias. Considerar ano com 365 dias e mês com 30 dias. */

public class IdadePessoaEmDias {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Informe sua idade em anos e meses, separados por espaço: ");
		
		int anos = sc.nextInt();
		int meses = sc.nextInt();
		int idadeEmDias = 0;
		
		idadeEmDias = anos * 365;
		idadeEmDias += meses * 30;
		
		System.out.println("Sua Idade em Dias: "+ idadeEmDias);
		
		sc.close();

	}

}
