package exercicios.iniciais.parte4;

import java.util.Locale;

/*ler um vetor de N números reais. Em seguida, mostrar na tela a média aritmética de todos
 *elementos. Depois mostrar todos os elementos do vetor que estejam abaixo da média. */

import java.util.Scanner;

public class MediaAritmetica {
	
	public static void main(String[] args) {
		
		Locale.setDefault(Locale.US);
		Scanner sc = new Scanner(System.in);
		
		int tamanhoVetor = tamanhoVetor(sc); 

		double[] numerosReais = coletaNumerosReais(tamanhoVetor, sc);
		
		double mediaAritmetica = calcularMediaAritmetica(tamanhoVetor, numerosReais);
		System.out.println(mediaAritmetica);
		
		mostrarNumerosAbaixoDaMediaAritmetica(tamanhoVetor, numerosReais, mediaAritmetica);
		
		sc.close();
	}
	
	private static int tamanhoVetor(Scanner sc) {
		int tamanhoVetor = sc.nextInt();
		return tamanhoVetor;
	}
	
	private static double[] coletaNumerosReais(int tamanhoVetor, Scanner sc) {

		double[] vetor = new double[tamanhoVetor];
		for (int i = 0; i < tamanhoVetor; i++) {
			vetor[i] = sc.nextDouble();
		}
		
		return vetor;
	}
	
	private static double calcularMediaAritmetica(int tamanhoVetor, double[] numerosReais) {
		
		double somaNumerosReais = 0.0;
		
		for (int i = 0; i < tamanhoVetor; i++) {
			somaNumerosReais += numerosReais[i];  
		}
		
		double mediaAritmetica = somaNumerosReais / tamanhoVetor;
		
		return mediaAritmetica;
	}
	
	private static void mostrarNumerosAbaixoDaMediaAritmetica(int tamanhoVetor, double[] numerosReais, double mediaAritmetica) {
		
		for (int i = 0; i < tamanhoVetor; i++) {
			if(numerosReais[i] < mediaAritmetica) {
				System.out.print(numerosReais[i] + "  ");
			}			
		}
		
	}
}
