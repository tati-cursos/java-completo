package exercicios.iniciais.parte4;

import java.util.Locale;
import java.util.Scanner;

public class MediaPonderada {

	public static void main(String[] args) {
		
		Locale.setDefault(Locale.US);
		Scanner sc = new Scanner(System.in);
		
		int qtdCasos;
		double nota1 = 0.0;
		double nota2 = 0.0;
		double nota3 = 0.0;
		double mediaPonderada = 0.0;
		int peso1 = 2;
		int peso2 = 3;
		int peso3 = 5;
		
		qtdCasos = sc.nextInt();
		
		for (int i = 0; i < qtdCasos; i++) {
			
			System.out.println("Informe as notas:");
			nota1 = sc.nextDouble();
			nota2 = sc.nextDouble();
			nota3 = sc.nextDouble();
			
			mediaPonderada = ((peso1 * nota1) + (peso2 * nota2) + (peso3 * nota3)) / (peso1 + peso2 + peso3);
			System.out.printf("Média: %.1f%n", mediaPonderada);
			System.out.println();
		}

		
		
		sc.close();

	}

}
