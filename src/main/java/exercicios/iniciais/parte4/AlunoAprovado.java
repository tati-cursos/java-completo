package exercicios.iniciais.parte4;

import java.util.Locale;
import java.util.Scanner;

/*ler um conjunto de N nomes de alunos, bem como as notas que eles tiraram no 1o e 2o semestres.
 *Cada uma dessas informações deve ser armazenada em um vetor. Depois, imprimir os nomes dos alunos aprovados,
 *considerando aprovados aqueles cuja média das notas seja maior ou igual a seis.
 */

public class AlunoAprovado {
	
	public static void main(String[] args) {
		
		Locale.setDefault(Locale.US);
		Scanner sc = new Scanner(System.in);
		
		System.out.print("Informe a quantidade de aluno: ");
		int quantidadeAluno = getQuantidadeAluno(sc);
		
		String[] nomeAlunos = new String[quantidadeAluno];
		double[] notaPrimeiroSemestre = new double[quantidadeAluno];
		double[] notaSegundoSemestre = new double[quantidadeAluno];
		
		System.out.println("Digite: Nome do aluno, Nota do 1º semestre e Nota do 2º semestre:");
		
		getNomeENotaAluno(sc, nomeAlunos, notaPrimeiroSemestre, notaSegundoSemestre);
		
		System.out.println("Aluno(s) Aprovado(s):");
		calculaMediaAluno(nomeAlunos, notaPrimeiroSemestre, notaSegundoSemestre);
		
		sc.close();
		
	}
	
	private static int getQuantidadeAluno(Scanner sc) {
		int quantidadeAluno = sc.nextInt();
		return quantidadeAluno;
	}
	
	private static void getNomeENotaAluno(Scanner sc, String[] nomeAlunos, double[] notaPrimeiroSemestre, double[] notaSegundoSemestre) {
		
		for (int i = 0; i < nomeAlunos.length; i++) {
			nomeAlunos[i] = sc.next();
			notaPrimeiroSemestre[i] = sc.nextDouble();
			notaSegundoSemestre[i] = sc.nextDouble();			
		}
	}
	
	private static void calculaMediaAluno(String[] nomeAlunos, double[] notaPrimeiroSemestre, double[] notaSegundoSemestre) {
		
		double mediaAluno = 0.0;
		
		for (int i = 0; i < nomeAlunos.length; i++) {
			mediaAluno = (notaPrimeiroSemestre[i] + notaSegundoSemestre[i]) / 2;

			if (mediaAluno >= 6.0) {
				System.out.println(nomeAlunos[i]);
			}
		}
	}

}
