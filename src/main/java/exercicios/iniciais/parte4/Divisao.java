package exercicios.iniciais.parte4;

import java.util.Scanner;

public class Divisao {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		int x;
		double numerador = 0;
		double denominador = 0;
		double resultadoDivisao = 0;
		
		x = sc.nextInt();
		
		for (int i = 0; i < x; i++) {
			System.out.println("Digite numerador e denominador: ");
			numerador = sc.nextDouble();
			denominador = sc.nextDouble();
			
			if (denominador != 0) {
				resultadoDivisao = numerador / denominador;
				System.out.println(resultadoDivisao);
			}
			else {
				System.out.println("Divisão impossível.");
			}
			
			System.out.println();
			
		}
		
		sc.close();

	}

}
