package exercicios.iniciais.parte4;

import java.util.Scanner;

public class Intervalo2 {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		int quantity;
		int number = 0;
		int somaIn = 0;
		int somaOut = 0;
		
		quantity = sc.nextInt();
		
		for (int i = 0; i < quantity; i++) {
			number = sc.nextInt();
			
			if(number >= 10 && number <= 20) {
				somaIn += 1;
			}
			else {
				somaOut += 1;
			}
		}
		
		System.out.println(somaIn + " in");
		System.out.println(somaOut + " out");
				
		sc.close();

	}

}
