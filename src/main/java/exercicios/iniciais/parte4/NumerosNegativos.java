package exercicios.iniciais.parte4;

import java.util.Scanner;

public class NumerosNegativos {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Digite qtd de numeros inteiros a serem lidos:");
		int numero = sc.nextInt(); 
		boolean contemNumeroNegativo = false;
		int[] numeros = new int[numero];
		
		System.out.println("informe os numeros:");
		
		for (int i = 0; i < numero; i++) {
			numeros[i] = sc.nextInt();
			if (numeros[i] < 0) {
				contemNumeroNegativo = true;
			}
		}
		
		if (contemNumeroNegativo) {
			for (int i = 0; i < numero; i++) {
				if (numeros[i] < 0) {
					System.out.println(numeros[i]);
				}
			}
		}
		else {
			System.out.println("Nenhum numero negativo");
		}
		
		
		
		sc.close();

	}

}
