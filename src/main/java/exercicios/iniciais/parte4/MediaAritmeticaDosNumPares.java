package exercicios.iniciais.parte4;

/* ler um vetor de N números inteiros. Em seguida, mostrar na tela a média aritmética somente
 * dos números pares lidos.*/

import java.util.Scanner;

public class MediaAritmeticaDosNumPares {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		int tamanhoVetor = getTamanhoVetor(sc);
		
		int[] numerosInteiros = coletarNumerosInteiros(tamanhoVetor, sc);
				
		int somaNumerosPares = somarNumPares(numerosInteiros);
		int qtdNumPares = getQtdNumPares(numerosInteiros);
		
		calcularMediaDosNumPares(somaNumerosPares, qtdNumPares);
		
		sc.close();

	}
	
	private static int getTamanhoVetor(Scanner sc) {
		int tamanhoVetor = sc.nextInt();
		return tamanhoVetor;
	}
	
	private static int[] coletarNumerosInteiros(int tamanhoVetor, Scanner sc) {
		
		int[] vet = new int[tamanhoVetor];
		for (int i = 0; i < vet.length; i++) {
			vet[i] = sc.nextInt();
		}
		return vet;
	}
	
	private static int somarNumPares(int[] numerosInteiros) {
		
		int somaNumerosPares = 0;
		
		for (int i = 0; i < numerosInteiros.length; i++) {
			if (isNumeroPar(numerosInteiros, i)) {
				somaNumerosPares += numerosInteiros[i];
			}
		}
		return somaNumerosPares;
	}

	private static int getQtdNumPares(int[] numerosInteiros) {

		int qtdNumPares = 0;

		for (int i = 0; i < numerosInteiros.length; i++) {
			if (isNumeroPar(numerosInteiros, i)) {
				qtdNumPares++;
			}
		}
		return qtdNumPares;
	}
	
	private static void calcularMediaDosNumPares(int somaNumerosPares, int qtdNumPares) {
		
		int mediaAritmeticaNumPares = somaNumerosPares / qtdNumPares;
		System.out.println(mediaAritmeticaNumPares);
	}
	
	private static boolean isNumeroPar(int[] numerosInteiros, int index) {
		return numerosInteiros[index] % 2 == 0;
	}

	
}
