package exercicios.iniciais.parte4;

public class FatorialSimples {

	public int calculaFatorial(int numeroAFatorar) {
		
		int fatorial = numeroAFatorar;
		int inicio = fatorial - 1;

		for (int i = inicio; i > 0; i--) {
			fatorial *= i;
		}

		return fatorial;
	}

}
