package exercicios.iniciais.parte4;

import java.util.Locale;
import java.util.Scanner;

public class NotaAluno {

	public static void main(String[] args) {
		
		Locale.setDefault(Locale.US);
		Scanner sc = new Scanner(System.in);
		
		System.out.print("Informe a quantidade de Aluno: ");
		int qtdAlunoNota = sc.nextInt();
		String[] alunos = new String[qtdAlunoNota];
		double[] notas = new double[qtdAlunoNota];
		boolean[] indiceAlunoReprovado = new boolean[qtdAlunoNota];
		double mediaDasNotas = 0.0;
		double somaNotas = 0.0;
		double menorNota = 10.0;
		String alunoMenorNota = "";
		double maiorNota = 0.0;
		String alunoMaiorNota = "";
		
		System.out.println("Digite nome do aluno e nota:");
		
		for (int i = 0; i < qtdAlunoNota; i++) {
			alunos[i] = sc.next();
			notas[i] = sc.nextDouble();
			
			somaNotas += notas[i];
			
			if (notaAbaixoDaMedia(notas[i])) {
				indiceAlunoReprovado[i] = true;
			}
			
			if (compararNota(notas[i], menorNota)) {
				menorNota = notas[i];
				alunoMenorNota = alunos[i];
			}
			
			if (compararNota(maiorNota, notas[i])) {
				maiorNota = notas[i];
				alunoMaiorNota = alunos[i];
			}
			
		}
		
		System.out.println("- Aluno(s) reprovado(s)");
		for (int i = 0; i < qtdAlunoNota; i++) {
			if (indiceAlunoReprovado[i]) {
				System.out.println(alunos[i] + ", nota " + notas[i]);
			}
		}
		
		System.out.println();
		System.out.println("- Aluno(s) aprovado(s)");
		for (int i = 0; i < qtdAlunoNota; i++) {
			if (indiceAlunoReprovado[i] == false) {
				System.out.println(alunos[i] + ", nota " + notas[i]);
			}
			
		}
		
		System.out.println();
		mediaDasNotas = somaNotas / qtdAlunoNota;
		System.out.printf("Média das notas: %.2f%n", mediaDasNotas);
		
		System.out.println();
		System.out.println("Aluno " + alunoMenorNota + ", menor nota: " + menorNota);
		System.out.println();
		System.out.println("Aluno " + alunoMaiorNota + ", maior nota: " + maiorNota);
				
		sc.close();
		
	}
	
	private static boolean notaAbaixoDaMedia (double notaAValidar) {
		
		if (notaAValidar < 6.0) {
			return true;
		} else {
			return false;
		}
		
	}
	
	private static boolean compararNota(double notaAValidar1, double notaAValidar2) {
		
		if (notaAValidar1 < notaAValidar2) {
			return true;
		}
		return false;
	}
		
}
