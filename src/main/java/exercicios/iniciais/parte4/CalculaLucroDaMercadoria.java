package exercicios.iniciais.parte4;

import java.util.Locale;
import java.util.Scanner;

public class CalculaLucroDaMercadoria {

	public static void main(String[] args) {
		
		Locale.setDefault(Locale.US);
		Scanner sc = new Scanner(System.in);
		
		System.out.print("Informe a quantidade de produto: ");
		
		int quantidadeProduto = getQuantidadeProduto(sc);
		String[] nomeProduto = new String[quantidadeProduto];
		double[] valorDeCompra = new double[quantidadeProduto];
		double[] valorDeVenda = new double[quantidadeProduto];
		int lucroAbaixoDe10Porcento = 0; 
		int lucroEntre10E20Porcento = 0;
		int lucroAcimaDe20Porcento = 0;
		double valorTotalCompra = 0.0;
		double valorTotalVenda = 0.0;
		double lucroTotal = 0.0;
		
		getDadosDoProduto(nomeProduto, valorDeCompra, valorDeVenda, sc);
			
		lucroAbaixoDe10Porcento = obterQuantidadeProdutosComLucroAbaixoDe10Porcento(valorDeCompra, valorDeVenda);
		System.out.println("Lucro abaixo de 10%: "+ lucroAbaixoDe10Porcento);
		
		lucroEntre10E20Porcento = obterQuantidadeProdutosComLucroEntre10E20Porcento(valorDeCompra, valorDeVenda);
		System.out.println("Lucro entre 10% e 20%: "+ lucroEntre10E20Porcento);
		
		lucroAcimaDe20Porcento = obterQuantidadeProdutosComLucroAcimaDe20Porcento(valorDeCompra, valorDeVenda);
		System.out.println("Lucro acima de 20%: "+ lucroAcimaDe20Porcento);
		
		valorTotalCompra = calcularValorTotalCompra(valorDeCompra);
		System.out.printf("Valor total de compra: %.2f%n", valorTotalCompra);
		
		valorTotalVenda = calcularValorTotalVenda(valorDeVenda);
		System.out.printf("Valor total de venda: %.2f%n", valorTotalVenda);
		
		lucroTotal = calcularLucroTotal(valorDeCompra, valorDeVenda);
		System.out.printf("Lucro total: %.2f", lucroTotal);
		
		sc.close();

	}
	
	private static int getQuantidadeProduto(Scanner sc) {
		
		int quantidadeProduto = sc.nextInt();
		return quantidadeProduto;
	}
	
	private static void getDadosDoProduto(String[] nomeProduto, double[] valorDeCompra, double[] valorDeVenda, Scanner sc) {
		
		for(int i = 0; i < nomeProduto.length; i++) {
			nomeProduto[i] = sc.next();
			valorDeCompra[i] = sc.nextDouble();
			valorDeVenda[i] = sc.nextDouble();
		}
	}
	
	private static int obterQuantidadeProdutosComLucroAbaixoDe10Porcento(double[] valorDeCompra, double[] valorDeVenda) {
		
		double lucroLiquido = 0.0;
		double _10PorcentoDaCompra = 0.0;
		int lucroAbaixoDe10Porcento = 0;
		
		for(int i = 0; i < valorDeCompra.length; i++) {
			lucroLiquido = valorDeVenda[i] - valorDeCompra[i];
			_10PorcentoDaCompra = valorDeCompra[i] * 0.1;
			
			if(lucroLiquido < _10PorcentoDaCompra) {
				lucroAbaixoDe10Porcento++;
			}
		}
		return lucroAbaixoDe10Porcento;
	}

	private static int obterQuantidadeProdutosComLucroEntre10E20Porcento(double[] valorDeCompra, double[] valorDeVenda) {

		double lucroLiquido = 0.0;
		double _10PorcentoDaCompra = 0.0;
		double _20PorcentoDaCompra = 0.0;
		int lucroEntre10E20Porcento = 0;

		for(int i = 0; i < valorDeCompra.length; i++) {
			lucroLiquido = valorDeVenda[i] - valorDeCompra[i];
			_10PorcentoDaCompra = valorDeCompra[i] * 0.1;
			_20PorcentoDaCompra = valorDeCompra[i] * 0.2;

			if(lucroLiquido >= _10PorcentoDaCompra && lucroLiquido <= _20PorcentoDaCompra) {
				lucroEntre10E20Porcento++;
			}
		}
		return lucroEntre10E20Porcento;
	}
	
	private static int obterQuantidadeProdutosComLucroAcimaDe20Porcento(double[] valorDeCompra, double[] valorDeVenda) {

		double lucroLiquido = 0.0;
		double _20PorcentoDaCompra = 0.0;
		int lucroAcimaDe20Porcento = 0;

		for(int i = 0; i < valorDeCompra.length; i++) {
			lucroLiquido = valorDeVenda[i] - valorDeCompra[i];
			_20PorcentoDaCompra = valorDeCompra[i] * 0.2;

			if(lucroLiquido > _20PorcentoDaCompra) {
				lucroAcimaDe20Porcento++;
			}
		}
		return lucroAcimaDe20Porcento;
	}
	
	private static double calcularValorTotalCompra(double[] valorDeCompra) {
		
		double valorTotalCompra = 0.0;
		
		for(int i = 0; i < valorDeCompra.length; i++) {
			valorTotalCompra += valorDeCompra[i];
		}
		return valorTotalCompra;
	}
	
	private static double calcularValorTotalVenda(double[] valorDeVenda) {

		double valorTotalVenda = 0.0;

		for(int i = 0; i < valorDeVenda.length; i++) {
			valorTotalVenda += valorDeVenda[i];
		}
		return valorTotalVenda;
	}
	
	private static double calcularLucroTotal(double[] valorDeCompra, double[] valorDeVenda) {

		double lucroTotal = 0.0;

		for(int i = 0; i < valorDeCompra.length; i++) {
			lucroTotal += (valorDeVenda[i] - valorDeCompra[i]);
		}
		return lucroTotal;
	}

}
