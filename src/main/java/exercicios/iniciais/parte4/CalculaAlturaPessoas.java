package exercicios.iniciais.parte4;

import java.util.Locale;
import java.util.Scanner;

public class CalculaAlturaPessoas {

	public static void main(String[] args) {
		
		Locale.setDefault(Locale.US);
		Scanner sc = new Scanner(System.in);
		
		int quantidadePessoas = getQuantidadePessoas(sc);
		double[] alturaPessoas = new double[quantidadePessoas];
		char[] sexoPessoas = new char[quantidadePessoas];
		double menorAltura = 0.0;
		double maiorAltura = 0.0;
		double mediaAlturaMulher = 0.0;
		int quantidadeHomens = 0;
		
		getAlturaESexoPessoas(sc, alturaPessoas, sexoPessoas);
		
		menorAltura = verificarMenorAltura(sc, alturaPessoas, sexoPessoas);
		System.out.print("Menor Altura: " + menorAltura);
		System.out.println();
		
		maiorAltura = verificarMaiorAltura(sc, alturaPessoas, sexoPessoas);
		System.out.print("Maior Altura: " + maiorAltura);
		System.out.println();
		
		mediaAlturaMulher = calculaMediaAlturaMulher(alturaPessoas, sexoPessoas);
		System.out.printf("Media altura das mulheres: %.2f", mediaAlturaMulher);
		System.out.println();
		
		quantidadeHomens = calculaQuantidadeHomem(sexoPessoas);
		System.out.print("Numero de homens: " + quantidadeHomens);
				
		sc.close();

	}
	
	private static int getQuantidadePessoas(Scanner sc) {
		
		System.out.print("Informe quantidade de pessoas: ");
		int quantidadePessoas = sc.nextInt();
		return quantidadePessoas;
	}
	
	private static void getAlturaESexoPessoas(Scanner sc, double[] alturaPessoas, char[] sexoPessoas) {
		
		System.out.println("Digite: Altura e Sexo de cada um:");
		
		for (int i = 0; i < alturaPessoas.length; i++) {
			alturaPessoas[i] = sc.nextDouble();
			sexoPessoas[i] = sc.next().charAt(0);
		}
	}
	
	private static double verificarMenorAltura(Scanner sc, double[] alturaPessoas, char[] sexoPessoas) {
		
		double menorAltura = alturaPessoas[0];

		for (int i = 0; i < alturaPessoas.length; i++) {
			if(alturaPessoas[i] < menorAltura) {
				menorAltura = alturaPessoas[i]; 
			}
		}
		return menorAltura;
	}
	
	private static double verificarMaiorAltura(Scanner sc, double[] alturaPessoas, char[] sexoPessoas) {

		double maiorAltura = alturaPessoas[0];

		for (int i = 0; i < alturaPessoas.length; i++) {
			if(alturaPessoas[i] > maiorAltura) {
				maiorAltura = alturaPessoas[i]; 
			}
		}
		return maiorAltura;
	}
	
	private static double calculaMediaAlturaMulher(double[] alturaPessoas, char[] sexoPessoas) {

		double somaAlturas = 0.0;
		double mediaAlturaMulher = 0.0;
		int quantidadeMulheres = 0;

		for (int i = 0; i < sexoPessoas.length; i++) {		
			if (sexoPessoas[i] == 'F' || sexoPessoas[i] == 'f') {
				somaAlturas += alturaPessoas[i];
				quantidadeMulheres++;
			}
		}
		return mediaAlturaMulher = (somaAlturas / quantidadeMulheres);
	}
	
	private static int calculaQuantidadeHomem(char[] sexoPessoas) {

		int quantidadeHomens = 0;

		for (int i = 0; i < sexoPessoas.length; i++) {		
			if (sexoPessoas[i] == 'M' || sexoPessoas[i] == 'm') {
				quantidadeHomens++;
			}
		}
		return quantidadeHomens;
	}

}
