package exercicios.iniciais.parte4;

import java.util.Scanner;

/*ler dois vetores A e B, contendo N elementos cada. Em seguida, gere um terceiro vetor C onde
 *cada elemento de C é a soma dos elementos correspondentes de A e B. Imprima o vetor C gerado.
 */

public class SomaDosElementos {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		int qtdNumerosInteiros = informaQtdNumerosInteiros(sc);
		int[] vetA = new int[qtdNumerosInteiros];
		int[] vetB = new int[qtdNumerosInteiros];
		
		coletarNumerosInteiros(qtdNumerosInteiros, vetA, sc);
		coletarNumerosInteiros(qtdNumerosInteiros, vetB, sc);
		somarExibir(qtdNumerosInteiros, vetA, vetB);
		
		sc.close();

	}
	
	private static int informaQtdNumerosInteiros(Scanner sc) {
		int qtdNumerosInteiros = sc.nextInt();
		return qtdNumerosInteiros;
	}
	
	private static void coletarNumerosInteiros(int qtdNumerosInteiros, int[] vet, Scanner sc) {

		for (int i = 0; i < qtdNumerosInteiros; i++) {
			vet[i] = sc.nextInt();
		}
	}
	
	private static void somarExibir(int qtdNumerosInteiros, int[] vetA, int[] vetB) {
		
		for (int i = 0; i < qtdNumerosInteiros; i++) {
			int somaDosVetores = vetA[i] + vetB[i];
			System.out.print(somaDosVetores + " ");
		}
	}

}
