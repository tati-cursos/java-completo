package exercicios.iniciais.parte4;

import java.util.Locale;
import java.util.Scanner;

/* Leia N números reais e armazene-os em um vetor. Em seguida, mostrar na tela o maior número
do vetor (supor não haver empates). Mostrar também a posição do maior elemento.
*/

public class MaiorNumero {

	public static void main(String[] args) {
		
		Locale.setDefault(Locale.US);
		Scanner sc = new Scanner(System.in);
		
		System.out.print("Informe a quantidade de números reais a serem lidos: ");
		int qtdNumerosReais = sc.nextInt();
		double[] numerosReais = new double[qtdNumerosReais];
		double maiorNumero = 0.0;
		int posicaoMaiorNumero = 0;
		
		for (int i = 0; i < qtdNumerosReais; i++) {
			numerosReais[i] = sc.nextDouble();
			
			if(numerosReais[i] > maiorNumero) {
				maiorNumero = numerosReais[i];
				posicaoMaiorNumero = i;
			}
		}
		
		System.out.printf("Maior número digitado: %.2f%n", maiorNumero );
		System.out.println("Posição do maior número digitado: "+ posicaoMaiorNumero);
		
		sc.close();

	}

}
