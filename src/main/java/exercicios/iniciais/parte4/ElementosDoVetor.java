package exercicios.iniciais.parte4;

import java.util.Locale;
import java.util.Scanner;

public class ElementosDoVetor {

	public static void main(String[] args) {
		
		Locale.setDefault(Locale.US);
		Scanner sc = new Scanner(System.in);
		
		int qtdNumerosReais = sc.nextInt();
		double[] numerosReais = new double[qtdNumerosReais];
		double somaNumerosReais = 0.0;
		double mediaNumerosReais = 0.0;
		
		for(int i = 0; i < qtdNumerosReais; i++) {
			numerosReais[i] = sc.nextDouble();
			somaNumerosReais += numerosReais[i];
		}
		
		for(int i = 0; i < qtdNumerosReais; i++) {
			System.out.printf("%.1f  ", numerosReais[i]);
		}
		
		System.out.println();
		System.out.printf("Soma: %.2f%n", somaNumerosReais);
		
		mediaNumerosReais = somaNumerosReais / qtdNumerosReais;
		System.out.printf("Media: %.2f ", mediaNumerosReais);
		
		sc.close();

	}

}
