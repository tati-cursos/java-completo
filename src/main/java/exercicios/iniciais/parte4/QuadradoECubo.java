package exercicios.iniciais.parte4;

import java.util.Scanner;

public class QuadradoECubo {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Digite um numero inteiro.");
		int entrada = sc.nextInt();
		int quadrado = 0;
		int cubo = 0;
		int linha = 0;
		
		for (int i = 1; i <= entrada; i++) {
			
			linha = i;
			quadrado = (int) Math.pow(i, 2);
			cubo = (int) Math.pow(i, 3);
			
			System.out.println(linha + " " + quadrado + " " + cubo);
		}
			
		sc.close();

	}

}
