package exercicios.iniciais.parte4;

import java.util.Scanner;

public class FatorialSimplesApp {
	
	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		FatorialSimples fatorialSimples = new FatorialSimples();
		while (true) {
			
			int numeroAFatorar = sc.nextInt();
			
			int fatorialCalculado = fatorialSimples.calculaFatorial(numeroAFatorar);
			
			System.out.println(fatorialCalculado);
			
			
		}

	}

}
