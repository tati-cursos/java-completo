package bank;

import java.util.Locale;
import java.util.Scanner;

import bank.entities.Account;

public class Program {

	public static void main(String[] args) {
		
		Locale.setDefault(Locale.US);
		Scanner sc = new Scanner(System.in);
		
		Account account;
		double depositValue = 0.0;
		double withdrawValue = 0.0;
		
		System.out.print("Enter account number: ");
		int accountNumber = sc.nextInt();
		System.out.print("Enter account holder: ");
		sc.nextLine();
		String accountHolder = sc.nextLine();
		System.out.print("Is there an initial deposit (y/n)? ");
		char initialDeposit = sc.next().charAt(0);

		if (initialDeposit == 'y' || initialDeposit == 'Y') {
			account = new Account(accountNumber, accountHolder, depositValue);
			System.out.print("Enter initial deposit value: ");
			depositValue = sc.nextDouble();
			account.deposit(depositValue);
		}
		else {
			account = new Account(accountNumber, accountHolder);
		}
		
		System.out.println();
		System.out.println("Account data:");
		System.out.println(account);
		
		System.out.println();
		System.out.print("Enter a deposit value: ");
		depositValue = sc.nextDouble();
		account.deposit(depositValue);
		
		System.out.println("Updated account data: ");
		System.out.println(account);
		System.out.println();
		
		System.out.print("Enter a withdraw value: ");
		withdrawValue = sc.nextDouble();
		account.withdraw(withdrawValue);
		
		System.out.println("Updated account data: ");
		System.out.println(account);
		
		sc.close();

	}

}
