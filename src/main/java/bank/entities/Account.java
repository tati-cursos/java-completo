package bank.entities;

public class Account {
	
	private static final double TAX = 5.00;
	
	private int accountNumber;
	private String accountHolder;
	private double accountBalance;
	
	public Account(int accountNumber, String accountHolder) {
		this.accountNumber = accountNumber;
		this.accountHolder = accountHolder;
	}
	
	public Account(int accountNumber, String accountHolder, double initialDeposit) {
		this.accountNumber = accountNumber;
		this.accountHolder = accountHolder;
		deposit(initialDeposit);
	}

	public String getAccountHolder() {
		return accountHolder;
	}

	public void setAccountHolder(String accountHolder) {
		this.accountHolder = accountHolder;
	}

	public int getAccountNumber() {
		return accountNumber;
	}

	public double getAccountBalance() {
		return accountBalance;
	}
	
	public void deposit(double depositValue) {
		accountBalance += depositValue;
	}
	
	public void withdraw(double withdrawValue) {
		accountBalance -= (withdrawValue + TAX);
	}

	public String toString() {
		return "Account " + accountNumber
		+ ", Holder: " + accountHolder
		+ ", Balance: $ "
		+ accountBalance;
	}
	
	
	

}
